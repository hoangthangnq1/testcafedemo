import { ClientFunction } from "testcafe";
import ProductPage from "../pages/ProductPage";
import HomePage from "../pages/HomePage";

const homeUrl = "https://tiki.vn/";

fixture("Home Page").page(homeUrl);
// .beforeEach(async t => {
//     await t
//     .maximizeWindow()
//     .wait(3000)
// });

test("verify product Page", async (t) => {
  HomePage.searchTiki("chuot");
  HomePage.clickBtnSearch();
  await t.wait(2000);
  ProductPage.clickBanChay();
  await t.wait(3000);
});
