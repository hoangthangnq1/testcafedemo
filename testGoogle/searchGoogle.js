import { Selector } from "testcafe";
fixture`test search`.page`https://www.google.com.vn/`;
const search = Selector("input[name='q']");
const button = Selector("input").withAttribute("value", "Tìm trên Google");;

test("Google test", async (t) => {
    const footer = Selector('div').withAttribute("role","navigation");
    function pages(i){
        Selector('a').withAttribute("arialabel","Page "+i);
    } 
    await t
        .maximizeWindow()
        .typeText(search,'chuot may tinh')
        .click(button)
        .scroll("bottom")
        .click(2)
        .wait(5000)
})