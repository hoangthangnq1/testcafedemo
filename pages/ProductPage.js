import { Selector, t } from "testcafe";

class ProductPage {
  constructor() {
    this.menuBanchay = Selector("a")
      .withAttribute("data-view-id", "search_sort_item")
      .nth(1);
    this.menuBanchay = Selector("a").withText("Bán Chạy");
    this.firstProduct = Selector("a")
      .withAttribute("class", "product-item")
      .nth(0);
  }

  async clickBanChay() {
    await t.click(this.menuBanchay);
  }
}
export default new ProductPage();

