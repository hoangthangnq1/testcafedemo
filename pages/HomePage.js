import {Selector, t} from "testcafe";

class HomePage{

    constructor(){
        // this.button = Selector("button").withAttribute("data-view-id","main_search_form_button");
        this.button = Selector("button").withExactText("Tìm Kiếm");
        this.searchBox = Selector("input[data-view-id='main_search_form_input']");
    
    }

    async searchTiki(search){
        await t
            .typeText(this.searchBox,search);
    }

    async clickBtnSearch(){
        await t
            .click(this.button)
    }


}
export default new HomePage();
