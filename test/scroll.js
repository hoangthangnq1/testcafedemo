import { Selector } from "testcafe";
import { ClientFunction } from 'testcafe';
fixture`test search`.page`https://tiki.vn/`;
const button = Selector("button[data-view-id='main_search_form_button']");
const menuBanchay = Selector("a").withAttribute("data-view-id", "search_sort_item").nth(1);
const scroll = ClientFunction(function() {
    window.scrollBy(0,1500);
});
test("scroll test", async (t) => {
  await t
    .maximizeWindow()
    .typeText("input[data-view-id='main_search_form_input']", "chuot")
    .click(button)
    .click(menuBanchay)
  await scroll(); 
})