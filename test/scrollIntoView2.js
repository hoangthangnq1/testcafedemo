import { Selector } from "testcafe";
import { ClientFunction } from 'testcafe';
fixture`test search`
  .page`https://tiki.vn/`;
  const scroll = ClientFunction(function() {
    window.scrollBy(0,1500);
});

test("My first test", async (t) => {
  await t.maximizeWindow();
  const target = await Selector("div").withText("Danh Mục Nổi Bật");
  await scroll();
  await t.click(target);
      //.scrollIntoView(target).wait(4000);
});
