import { Selector } from "testcafe";
fixture`test search`.page`https://tiki.vn/`;

const button = Selector("button[data-view-id='main_search_form_button']");
const firstProduct = Selector("a").withAttribute("class", "product-item").nth(0);
const menuBanchay = Selector("a").withAttribute("data-view-id", "search_sort_item").nth(1);
const productTitle = Selector("h1").withAttribute("class","title");
test("My first test", async (t) => {
  await t
    .maximizeWindow()
    .typeText("input[data-view-id='main_search_form_input']", "chuot")
    .click(button)
    .click(menuBanchay)
    .click(firstProduct)
    .expect(productTitle.innerText).eql('Chuột Không Dây Sạc Điện Imice E-1300 Không Tạo Tiếng Ồn Siêu Tiết Kiệm Pin Có Thể Kết Nối Bloetooth Với Điện Thoại Table Khoản Cách Hoạt Động 10m – Hàng Chính Hãng')
    .wait(5000);   
})


