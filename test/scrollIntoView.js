import { Selector } from "testcafe";
fixture`test search`.page`https://tiki.vn/`;
const button = Selector("button[data-view-id='main_search_form_button']");
const menuBanchay = Selector("a")
  .withAttribute("data-view-id", "search_sort_item")
  .nth(1);
const target = Selector("a").withAttribute("class", "product-item").nth(17);
//Selector(".product-item:last-child")
test("My first test", async (t) => {
  await t
    .maximizeWindow()
    .typeText("input[data-view-id='main_search_form_input']", "chuot")
    .click(button)
    .click(menuBanchay);
  await t.scrollIntoView(target).click(target);
});
