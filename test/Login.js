import { Selector } from "testcafe";
fixture`test Tiki`.page`https://tiki.vn/`;
const login = Selector("div").withAttribute("data-view-id","header_header_account_container").withText("Đăng Nhập");
const btnTieptuc = Selector("button").withText("Tiếp Tục");
test("login test", async (t) => {
  await t
    .maximizeWindow()
    .click(login)
    .typeText("input[placeholder='Số điện thoại']","0853941566")
    .click(btnTieptuc)
    .wait(5000);
});